<?php

/**
 * @file
 * uw_auth_required_cas.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_auth_required_cas_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission.
  $overrides["user_permission.access content.roles|anonymous user"]["DELETED"] = TRUE;

  // Exported overrides for: variable.
  $overrides["variable.service_links_category_vocs.value|12"] = 0;
  $overrides["variable.service_links_category_vocs.value|13"] = 0;
  $overrides["variable.service_links_category_vocs.value|2"]["DELETED"] = TRUE;
  $overrides["variable.service_links_category_vocs.value|3"]["DELETED"] = TRUE;
  $overrides["variable.service_links_category_vocs.value|4"]["DELETED"] = TRUE;
  $overrides["variable.service_links_category_vocs.value|5"]["DELETED"] = TRUE;
  $overrides["variable.service_links_category_vocs.value|6"]["DELETED"] = TRUE;
  $overrides["variable.service_links_category_vocs.value|7"]["DELETED"] = TRUE;
  $overrides["variable.service_links_node_types.value|uw_blog"] = 0;
  $overrides["variable.service_links_node_types.value|uw_event"] = 0;
  $overrides["variable.service_links_node_types.value|uw_image_gallery"] = 0;
  $overrides["variable.service_links_node_types.value|uw_news_item"] = 0;
  $overrides["variable.service_links_node_types.value|uw_project"] = 0;
  $overrides["variable.service_links_node_types.value|uw_service"] = 0;
  $overrides["variable.service_links_node_types.value|uw_web_page"] = 0;
  $overrides["variable.service_links_node_view_modes.value"] = '';
  $overrides["variable.xmlsitemap_settings_menu_link_main-menu.value|status"] = 0;
  $overrides["variable.xmlsitemap_settings_node_uw_blog.value|status"] = 0;
  $overrides["variable.xmlsitemap_settings_node_uw_ct_person_profile.value|status"] = 0;
  $overrides["variable.xmlsitemap_settings_node_uw_event.value|status"] = 0;
  $overrides["variable.xmlsitemap_settings_node_uw_image_gallery.value|status"] = 0;
  $overrides["variable.xmlsitemap_settings_node_uw_news_item.value|status"] = 0;
  $overrides["variable.xmlsitemap_settings_node_uw_special_alert.value|status"] = 1;
  $overrides["variable.xmlsitemap_settings_node_uw_web_form.value|status"] = 0;
  $overrides["variable.xmlsitemap_settings_node_uw_web_page.value|status"] = 0;

  return $overrides;
}
